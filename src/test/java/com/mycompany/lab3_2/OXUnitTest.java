/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3_2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    //TDD 
    
    @Test   
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"1","2","3"},{"O","O","O"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow3_O_output_true(){
        String[][] table = {{"1","2","3"},{"4","5","6"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow1_O_output_false(){
        String[][] table = {{"O","O","3"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckWinCol1_O_output_true(){
        String[][] table = {{"1","O","3"},{"O","5","6"},{"O","8","9"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol2_O_output_true(){
        String[][] table = {{"O","2","3"},{"O","O","6"},{"7","O","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckWinCol3_O_output_false(){
        String[][] table = {{"1","2","O"},{"4","5","O"},{"7","8","O"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    
    @Test   
    public void testCheckDiag_output_true(){
        String[][] table = {{"O","2","3"},{"4","O","6"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test   
    public void testCheckAntiDiag_output_true(){
        String[][] table = {{"1","2","O"},{"4","O","6"},{"O","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test   
    public void testCheckDraw_output_true(){
        int Turn = 9;
        boolean result = OX.checkDraw(Turn);
        assertEquals(true,result);
    }
    
    @Test   
    public void testCheckDraw_output_false(){
        int Turn = 5;
        boolean result = OX.checkDraw(Turn);
        assertEquals(false,result);
    }
    
    
}


