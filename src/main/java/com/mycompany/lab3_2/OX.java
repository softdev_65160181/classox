/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3_2;

/**
 *
 * @author User
 */
class OX {
    
    
    static boolean checkWin(String[][] table , String currentPlayer){
        for(int row=0; row<3; row++){
            if(checkRow(table,currentPlayer , row)){
                return true;
            }
        }
        return false;
    }

        
    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }
    

    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (table[0][col].equals(currentPlayer)
                    && table[1][col].equals(currentPlayer)
                    && table[2][col].equals(currentPlayer)) {
                return true;
            }
        }
        return false;
    }

    
    private static boolean checkDiag(String[][] table, String currentPlayer) {
        return (table[0][0].equals(currentPlayer)&& table[1][1].equals(currentPlayer)&& table[2][2].equals(currentPlayer));
    }


    private static boolean checkAntiDiag(String[][] table, String currentPlayer) {
        return (table[0][2].equals(currentPlayer)
                && table[1][1].equals(currentPlayer)
                && table[2][0].equals(currentPlayer));
    }


    static boolean checkDraw(int Turn) {
        return Turn == 9;
    }
   
    }   
    
    
    

